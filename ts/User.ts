export class User {
    public familyName: string
    public giveName: string
    public age: number

    constructor(familyName: string, giveName: string, age: number) {
        this.familyName = familyName
        this.giveName = giveName
        this.age = age
    }
}
