import {User} from "./User"
import {v4 as uuid} from "uuid"

const user = new User("ほげ", "ふが", 28)

const contentsElem = document.getElementById("contents")
if (!!contentsElem) {
    contentsElem.innerText = `${user.familyName} ${user.giveName}(${user.age})`
}

const saibanButton = document.getElementById("saiban") as HTMLButtonElement
const uuidSpan = document.getElementById("uuid") as HTMLButtonElement
saibanButton.onclick = (_) => {
    uuidSpan.innerText = uuid();
}
